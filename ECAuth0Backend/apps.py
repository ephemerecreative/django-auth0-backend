from django.apps import AppConfig


class Auth0BackendConfig(AppConfig):
    name = 'ECAuth0Backend'
