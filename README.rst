=======================
Auth0Backend For Django
=======================

Source_

*USE AT YOUR OWN RISK. API WILL LIKELY KEEP CHANGING FOR NOW.*

Experimental application to manage code and jwt auth from Auth0 with Django.

If you're using the JWT mechanism, you'll need to include the `openid email` scopes to get the email_verified claim.

This app provides a custom user model which is required for use with the app (see step 2 of setup).

Setup
-----

1. Add to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'ECAuth0Backend',
    ]

2. Set django to use the ECAuth0Backend user model::

    AUTH_USER_MODEL = 'ECAuth0Backend.A0User'

3. Include the URLconf in your project urls.py like this::

    import ECAuth0Backend.urls

    urlpatterns = [
        # your urls
    ] + ECAuth0Backend.urls.urlpatterns

4. Run `python manage.py migrate` to create the user model.

5. Add the required configuration to your settings.py (you can look through ECAuth0Backend.settings.py for more settings)::

    AUTH0_DOMAIN = '' # Your auth0 domain

    AUTH0_CODE_CLIENT_ID = '' # Your auth0 client ID for code auth
    AUTH0_CODE_CLIENT_SECRET = '' # Your auth0 client secret for code auth
    AUTH0_CODE_CALLBACK_PATH = '' # Callback url you want auth0 to send the client back to (url pattern defined below)

    AUTH0_JWT_SECRET = '' # auth0 client secret for jwt client
    AUTH0_JWT_CLIENT_ID = '' # auth0 client id for jwt client

6. Add the authentication backend::

    AUTHENTICATION_BACKENDS = [
        'ECAuth0Backend.backend.Auth0Backend',
        'django.contrib.auth.backends.ModelBackend',
    ]

7. For JWT with DRF use ECAuth0Backend.jwt.Auth0JWTAuthentication as an authentication class

.. _Source: https://bitbucket.org/ephemerecreative/django-auth0-backend